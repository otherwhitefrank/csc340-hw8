/*
Name: LinkedList.h
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Definition of a single linked LinkedList class LinkedList
*/


#ifndef _LINKEDList_H
#define _LINKEDList_H

#include "ListExceptions.h"
#include "ListNode.h"
#include "ListIterator.h"

/** @class LinkedList
* ADT LinkedList - Pointer-based implementation. */
class LinkedList
{
public:
	friend class ListIterator;
	// Constructors and destructor:

	/** Default constructor. */
	LinkedList();

	/** Copy constructor.
	* @param aLinkedList The LinkedList to copy. */
	LinkedList(const LinkedList& aLinkedList);

	//overload the assignment operator
	LinkedList operator=(const LinkedList&  rhs);
	/** Destructor. */
	~LinkedList();

	// LinkedList operations:
	bool isEmpty() const;
	int getLength() const;
	void insert(int index, const ListItemType& newItem)
		throw(ListIndexOutOfRangeException, ListException);
	void remove(int index)
		throw(ListIndexOutOfRangeException);
	void retrieve(int index, ListItemType& dataItem) const
		throw(ListIndexOutOfRangeException);

	
	
	void reverse(); //Reverses LinkedList

	void clearList(); //Wipes LinkedList

	//overload the << operator: idea is similar to the find()
	friend ostream & operator << (ostream & out, LinkedList& aLinkedList);
protected:
 //Make these protected so SortedLinkedList can access them as a child

	/** Number of items in LinkedList. */
	int      size;
	/** Pointer to linked LinkedList of items. */
	ListNode *head;

	//Print function used by operator <<
	void PrintList(ostream& out);

private:

	
	//Helper function for reverse
	ListNode* reverseHelper(ListNode* cur, ListNode* prev);


	/** Locates a specified node in a linked LinkedList.
	* @pre index is the number of the desired node.
	* @post None.
	* @param index The index of the node to locate.
	* @return A pointer to the index-th node. If index < 1
	*        or index > the number of nodes in the LinkedList,
	*        returns NULL. */
	ListNode *find(int index) const;
}; // end LinkedList
// End of header file.

#endif