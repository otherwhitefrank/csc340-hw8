/*
Name: CSC340-HW8.h
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Driver to test out LinkedList ADT
*/

#include "LinkedList.h"
#include "SortedLinkedList.h"

#include <iostream>
#include <random>

//Helper function that returns a random number between a and b
float rand_FloatRange(float a, float b)
{
	return ((b-a)*((float)rand()/RAND_MAX))+a;
}


int main(char* argv)
{
	LinkedList a, b;
	SortedLinkedList c;

	int stopExec = 0;

	for (int i = 0; i < 30; i++)
	{
		a.insert(1, i);
	}

	cout << "a: " << a;

	cout << "\n";

	cout << "b: " << b;

	cout << "\n";

	cout << "reversing a: ";
	a.reverse();

	cout << a;

	cout << "\n";
	
	b = a;
	
	cout << b;
	
	cout << "\n";

	cout << "reversing a again: " ;
	a.reverse();
	cout << a;

	cout << "\n";

	cout << "QUESTION 2: " << "\n";
	cout << "Entering numbers in random order: ";
	for (int i = 1; i < 30; i++)
	{
		int num = (int) rand_FloatRange(-4*i, i*4);
		c.insert(num);
		cout << num << " ";
	}

	cout << "\n";
	cout << "Inorder in SortedLinkedList c: " << c;

	cout << "\n";

	cout << "Inserting 32, 12, 1: ";
	c.insert(32);
	c.insert(12);
	c.insert(1);

	cout << c;
	int num1 = 32;
	cout << "\n" << "Does the list contain 32?: ";
	
	if (c.retrieve(num1))
		cout << "Yes" << "\n";
	else
		cout << "No" << "\n";


	cin >> stopExec;


	return 0;
}