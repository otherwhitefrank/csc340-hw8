/*
Name: ListNode.h
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: define the data structure ListNode that will be used by both the ListIterator and LinkedList class
*/

#ifndef _LIST_NODE_ITERATOR
#define _LIST_NODE_ITERATOR

#include <iostream> //Brings across NULL, needed for standard constructor

typedef int ListItemType;

class ListNode
{
public:

	ListNode(ListItemType itemVal, ListNode *nodePtr):item(itemVal),next(nodePtr){}; //Standard Constructor with values
	ListNode():item(0),next(NULL){}; //Default constructor
	
	ListItemType item; //Current Item
	ListNode *next; //Next item
};

#endif