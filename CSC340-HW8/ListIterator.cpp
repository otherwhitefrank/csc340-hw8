/*
Name: ListIterator.h
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Implement an iterator for the LinkedList class
*/

#include "ListIterator.h"

ListIterator::ListIterator(const LinkedList *aList, ListNode *nodePtr)
{
	container = aList; //Set the current container to the passed in list
	cur = container->head;
}


const ListItemType& ListIterator::operator*()
{
	return cur->item;
}

ListIterator ListIterator::operator++()    // prefix ++
{
	cur = cur->next; //Advance the iterator
	return *this;

}


bool ListIterator::operator==(const ListIterator& rhs) const
{
	return rhs.cur->item == cur->item; //Return if the two items are equal

}

bool ListIterator::operator!=(const ListIterator& rhs) const
{
	return ! rhs.cur->item == cur->item; //Return if the two items are unequal

}

