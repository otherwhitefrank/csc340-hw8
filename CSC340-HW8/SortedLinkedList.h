/*
Name: SortedLinkedList.h
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Definition of a sorted (inorder) single linked LinkedList inherits class LinkedList
*/


#ifndef _SORTEDLINKEDList_H
#define _SORTEDLINKEDList_H

#include "LinkedList.h"

class SortedLinkedList : LinkedList
{
public:

	void insert(const ListItemType& newItem); //Redefined to insert an item inorder in our linked list
	void remove(const ListItemType& newItem); //This function removes the node that contains newItem.

	bool retrieve(ListItemType& dataItem) const; //Returns true if there exists an item in our list
	ListNode* find(ListItemType& dataItem) const; //Returns a link to the node if its present in our linked list, otherwise it returns NULL

	//overload the assignment operator
	using LinkedList::operator=;

	//overload the << operator locally so streams work
	friend ostream & operator << (ostream & out, SortedLinkedList& aLinkedList);

	//Print function used by operator <<
	void PrintList(ostream& out);

private:
	

	int getInsertionIndex(const ListItemType& newItem); //Helper function to return index to insert item at, used in new inorder insert function
	int getFindIndex(const ListItemType& newItem); //Helper function to return removal index to call parent remove function
};

#endif

