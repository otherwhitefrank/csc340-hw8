/*
Name: ListIterator.h
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Define an iterator for the List class
*/

#ifndef _LIST_ITERATOR_H
#define _LIST_ITERATOR_H

#include "ListNode.h" // Definition of ListNode and

// ListItemType; ListNode declares
// ListIterator as a friend class

#include "LinkedList.h"


/** @class ListIterator
*  Used in the iterator version of the ADT list. */
class ListIterator 
{
public:
	friend class LinkedList;

	ListIterator(const LinkedList *aList, ListNode *nodePtr);

	const ListItemType& operator*();
	ListIterator operator++();    // prefix ++

	bool operator==(const ListIterator& rhs) const;
	bool operator!=(const ListIterator& rhs) const;

private:
	const LinkedList *container; // ADT associated with iterator
	ListNode *cur;        // current location in collection
}; // end ListIterator

#endif