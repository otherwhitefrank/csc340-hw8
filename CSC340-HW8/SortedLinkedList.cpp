/*
Name: SortedLinkedList.cpp
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Implementation of a sorted (inorder) single linked LinkedList inherits class LinkedList
*/

#include "SortedLinkedList.h"

void SortedLinkedList::insert(const ListItemType& newItem) //Redefined to insert an item inorder in our linked list
{
	LinkedList::insert(getInsertionIndex(newItem), newItem); //Call parent insert function with our found index.
}

int SortedLinkedList::getInsertionIndex(const ListItemType& newItem) //Private function to return correct insertion index for inorder insert function
{
	int currIndex = 1; //LinkedList parent starts its index at 1

	ListNode* tempNode = this->head;
	while (tempNode != NULL && newItem > tempNode->item)
	{
		currIndex++; //Cycle through the LinkedList till we get to an item that is greater then newItem 
		tempNode = tempNode->next;
	}

	return currIndex;
}



void SortedLinkedList::remove(const ListItemType& newItem) //This function removes the node that contains newItem.
{
	ListItemType tempItem = newItem;
	int removeIndex = getFindIndex(tempItem);
	LinkedList::remove(removeIndex);
}



bool SortedLinkedList::retrieve(ListItemType& dataItem) const//Returns true if there exists an item in our list
{
	bool itemFound = false;

	ListNode* tempNode = this->head;
	while (tempNode != NULL && itemFound == false) //While we have more items and the item has not been found
	{
		if (tempNode->item == dataItem)
			itemFound = true;

		tempNode = tempNode->next;
	}

	return itemFound;
}

int SortedLinkedList::getFindIndex(const ListItemType& newItem)
{
	ListItemType searchItem = newItem;

	int currIndex = 1; //LinkedList parent starts its index at 1

	if (retrieve(searchItem)) //If the item is present in our list
	{

		ListNode* tempNode = this->head;
		while (tempNode != NULL && newItem != tempNode->item) //We haven't hit the end of our list and have not found the index yet
		{
			currIndex++; //Cycle through the LinkedList till we get to an item that is greater then newItem 
			tempNode = tempNode->next;
		}

	}
	else
	{
		throw ListException("Item not in list!");
	}

	return currIndex;

}

ListNode* SortedLinkedList::find(ListItemType& dataItem) const //Returns a link to the node if its present in our linked list, otherwise it returns NULL
{

	ListNode* tempNode = NULL;

	if (retrieve(dataItem)) //If the item is present in our list
	{

		tempNode = this->head;
		while (tempNode != NULL && dataItem != tempNode->item) //We haven't hit the end of our list and have not found the index yet
		{
			tempNode = tempNode->next;
		}
		return tempNode;

	}
	else
	{
		return NULL;
	}
}


void SortedLinkedList::PrintList(ostream& out)
{
	LinkedList::PrintList(out); //Call parent function
}

//overload the << operator: idea is similar to the find()
ostream& operator<< (ostream & out, SortedLinkedList & aLinkedList)
{
	aLinkedList.PrintList(out);

	return out;
}
