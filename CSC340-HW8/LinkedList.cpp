/*
Name: LinkedList.cpp
Copyright: Frank Dye, 11/25/2013
Author: Frank Dye
Date: 11/25/2013
Description: Implementation of a single linked List class LinkedList
*/

#include "ListNode.h"    // header file for ListNode 
#include "LinkedList.h"

using namespace std;

LinkedList::LinkedList():size(0),head(NULL) //initializer or initialization segment
{
}

LinkedList::LinkedList(const LinkedList& aLinkedList)
	: size(aLinkedList.size)
{
	if (aLinkedList.head == NULL){
		head = NULL;  // original LinkedList is empty
		size = 0;
	}
	else
	{  // copy first node
		head = new ListNode;
		head->item = aLinkedList.head->item;

		// copy rest of LinkedList
		ListNode *newPtr = head;  // new LinkedList pointer
		// newPtr points to last node in new LinkedList
		// origPtr points to nodes in original LinkedList
		for (ListNode *origPtr = aLinkedList.head->next;
			origPtr != NULL;
			origPtr = origPtr->next)
		{  newPtr->next = new ListNode;
		newPtr = newPtr->next;
		newPtr->item = origPtr->item;
		}  // end for

		newPtr->next = NULL;
	}  // end if
}  // end copy constructor

LinkedList::~LinkedList()
{
	//while (!isEmpty())
	//  remove(1);
	ListNode *curr=head;
	while (curr!=NULL){
		head = curr->next;
		delete curr;
		curr = head;
	}

}  // end destructor

bool LinkedList::isEmpty() const
{
	return size == 0;
}  // end isEmpty

int LinkedList::getLength() const
{
	return size;
}  // end getLength

ListNode* LinkedList::find(int index) const
{
	if ( (index < 1) || (index > getLength()) )
		return NULL;

	else  // count from the beginning of the LinkedList.
	{  ListNode *cur = head;
	for (int skip = 1; skip < index; ++skip)
		cur = cur->next;
	return cur;
	}  // end if

}  // end find

void LinkedList::retrieve(int index,
					ListItemType& dataItem) const
					throw(ListIndexOutOfRangeException)
{
	if ( (index < 1) || (index > getLength()) )
		throw ListIndexOutOfRangeException(
		"ListIndexOutOfRangeException: retrieve index out of range");
	else
	{  // get pointer to node, then data in node
		ListNode *cur = find(index);
		dataItem = cur->item;
	}  // end if
}  // end retrieve

void LinkedList::insert(int index, const ListItemType& newItem)
	throw(ListIndexOutOfRangeException, ListException)
{
	int newLength = getLength() + 1;

	if ( (index < 1) || (index > newLength) )
		throw ListIndexOutOfRangeException(
		"ListIndexOutOfRangeException: insert index out of range");
	else
	{  // try to create new node and place newItem in it
		try
		{
			ListNode *newPtr = new ListNode;
			size = newLength;
			newPtr->item = newItem;

			// attach new node to LinkedList
			if (index == 1)
			{  // insert new node at beginning of LinkedList
				newPtr->next = head;
				head = newPtr;
			}
			else
			{  ListNode *prev = find(index-1);
			// insert new node after node
			// to which prev points
			newPtr->next = prev->next;
			prev->next = newPtr;
			}  // end if
		}  // end try
		catch (bad_alloc e)
		{
			throw ListException(
				"ListExceptionn: memory allocation failed on insert");
		}  // end catch
	}  // end if
}  // end insert

void LinkedList::remove(int index) throw(ListIndexOutOfRangeException)
{
	ListNode *cur;

	if ( (index < 1) || (index > getLength()) )
		throw ListIndexOutOfRangeException(
		"ListIndexOutOfRangeException: remove index out of range");
	else
	{  --size;
	if (index == 1)
	{  // delete the first node from the LinkedList
		cur = head;  // save pointer to node
		head = head->next;
	}

	else
	{  ListNode *prev = find(index - 1);
	// delete the node after the node to which prev points
	cur = prev->next;  // save pointer to node
	prev->next = cur->next;
	}  // end if

	// return node to system
	cur->next = NULL;
	delete cur;
	cur = NULL;
	}  // end if
}  // end remove


void LinkedList::PrintList(ostream& out)
{
	ListNode* curr;
	curr = this->head;

	while (curr != NULL)
	{
		out << curr->item << " ";
		curr = curr->next;
	}
}

//overload the assignment operator
LinkedList LinkedList::operator=(const LinkedList&  rhs)
{
	int newSize = rhs.size;

	this->clearList();
	

	for (int i = 1 ; i < newSize+1; i++)
	{
		ListItemType a;
		rhs.retrieve(i, a);
		this->insert(i, a);
	}

	return *this;
}


void LinkedList::clearList() //Wipes LinkedList
{
	ListNode* cur = this->head;
	ListNode* prev = NULL;
	while (cur != NULL)
	{
		prev = cur;
		cur = cur->next;
		delete prev;
	}
	size = 0;

}

//overload the << operator: idea is similar to the find()
ostream& operator<< (ostream & out, LinkedList & aLinkedList)
{
	aLinkedList.PrintList(out);

	return out;
}


//Reverses LinkedList
void LinkedList::reverse()
{
	this->head = reverseHelper(this->head, NULL); //Trigger our helper function with the head and a previous node of null
}

ListNode* LinkedList::reverseHelper(ListNode* cur, ListNode* prev)
{
	ListNode* first; //Temp holder for the past back first node
	if (cur->next != NULL)
	{
		first = reverseHelper(cur->next, cur); //Pass first node up the line
		cur->next = prev; //Change pointer to previous node
	}
	else
	{
		cur->next = prev;
		first = cur; //This is the last node of the list, our new head, so pass it up the line
	}

	return first;
}